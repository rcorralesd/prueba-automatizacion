#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Simulador de Canon Financiero
  Verificar el funcionamiento de la pantalla de simulación de Canon Financiero
  expuesta por el grupo Bancolombia - Leasing, en cuanto a la presentación de los
  valores calculados.El usuario debe poder ingresar al formulario los datos requeridos.
  
  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario de simulación de Canon Financiero
    Given Abrir el navegador con la url dada e ingresar a la seccion productos y servicios 
    And   Seleccionar la opción leasing
    And   Seleccionar la opción leasing habitacional
    And   Dar click al botón Simular Canon Constante
    When  Diligencio el simulador de canon financiero
    |Valor Activo| Plazo | Porcentaje| Tipo Tasa| 
    |11000000    | 14    | 9         | DTF      |
    |18000000    | 25    | 6         | IBR TA      |
    Then  Verifico diligenciamiento exitoso
    
  @CasoAlterno
   Scenario: Diligenciamiento con errores del formulario de simulación de Canon Financiero
    Given Abrir el navegador con la url dada e ingresar a la seccion productos y servicios 
    And   Seleccionar la opción leasing
    And   Seleccionar la opción leasing habitacional
    And   Dar click al botón Simular Canon Constante
    When  Diligencio el simulador de canon financiero
    |Valor Activo| Plazo | Porcentaje| Tipo Tasa| 
    |12000000    |   13  |     8     | DTF      |
    Then  Verifico que se presente alerta de validación
