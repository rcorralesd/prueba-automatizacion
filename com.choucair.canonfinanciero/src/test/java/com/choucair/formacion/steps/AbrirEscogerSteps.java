package com.choucair.formacion.steps;

import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.AbrirNavegadorPage;

public class AbrirEscogerSteps {

	AbrirNavegadorPage AbrirNavegadorPage;
	
	@Step
	public void abrir_y_opcion_prod_y_ser() { 
		
		AbrirNavegadorPage.open(); 
		AbrirNavegadorPage.opcion_prod_y_ser();
		
    }
	
	
	@Step
	public void abrir_y_validar_opcion_leasing() { 
		
		AbrirNavegadorPage.opcion_leasing();
		AbrirNavegadorPage.validar_leasing();
		                                     
	}
	
	@Step
	public void abrir_y_validar_opcion_leasing_habitacional() { 
		
		AbrirNavegadorPage.opcion_leasing_habitacional();
		AbrirNavegadorPage.validar_leasing_habitacional();
		                                     
	}
	
	@Step
	public void abrir_validar_canon_constante() { 
		
		AbrirNavegadorPage.opcion_canon_constante();
		AbrirNavegadorPage.validar_canon_constante();
		                                     
	}
	
	
	
	
}
