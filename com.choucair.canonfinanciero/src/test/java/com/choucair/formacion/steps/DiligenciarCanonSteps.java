package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.DiligenciarCanonPage;

import net.thucydides.core.annotations.Step;



public class DiligenciarCanonSteps {
	
DiligenciarCanonPage DiligenciarCanonPage;

	
	@Step
	public void diligenciar_canon_financiero_tabla (List<List<String>> data, int id) {
		
		DiligenciarCanonPage.ValorActivo(data.get(id).get(0).trim());
		DiligenciarCanonPage.Plazo(data.get(id).get(1).trim());
		DiligenciarCanonPage.Porcentaje(data.get(id).get(2).trim());
		DiligenciarCanonPage.Select_Tipo_Tasa(data.get(id).get(3).trim());
		DiligenciarCanonPage.Simular();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_exitoso(){
		DiligenciarCanonPage.diligenciamiento_sin_errores();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_con_errores(){
		DiligenciarCanonPage.diligenciamiento_con_errores();
	}
	
	

}
