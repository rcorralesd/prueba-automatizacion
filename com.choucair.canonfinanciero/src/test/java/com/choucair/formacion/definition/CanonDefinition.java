package com.choucair.formacion.definition;

import net.thucydides.core.annotations.Steps;
import com.choucair.formacion.steps.AbrirEscogerSteps;
import com.choucair.formacion.steps.DiligenciarCanonSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import java.util.List;

public class CanonDefinition {
	
	@Steps
	AbrirEscogerSteps AbrirEscogerSteps;
	
	@Steps
	DiligenciarCanonSteps DiligenciarCanonSteps;
	
	
	@Given("^Abrir el navegador con la url dada e ingresar a la seccion productos y servicios$")
	public void abrir_el_navegador_con_la_url_dada_e_ingresar_a_la_seccion_productos_y_servicios(){
	    
		AbrirEscogerSteps.abrir_y_opcion_prod_y_ser();
	}

	@Given("^Seleccionar la opción leasing$")
	public void seleccionar_la_opción_leasing(){
		
		AbrirEscogerSteps.abrir_y_validar_opcion_leasing();
	    
	}

	@Given("^Seleccionar la opción leasing habitacional$")
	public void seleccionar_la_opción_leasing_habitacional(){
	    
		AbrirEscogerSteps.abrir_y_validar_opcion_leasing_habitacional();
	}

	@Given("^Dar click al botón Simular Canon Constante$")
	public void dar_click_al_botón_Simular_Canon_Constante(){
		
		AbrirEscogerSteps.abrir_validar_canon_constante();
	    
	}

	@When("^Diligencio el simulador de canon financiero$")
	public void diligencio_el_simulador_de_canon_financiero(DataTable dtDatosForm) {
	   
		 List<List<String>> data = dtDatosForm.raw();
		   
		   for (int i=1; i<data.size(); i++) {
			   DiligenciarCanonSteps.diligenciar_canon_financiero_tabla(data, i);
			   try {
				   Thread.sleep(5000);//Espera entre ejecuciones en milisegundos
			   }catch(InterruptedException e) {}
		   }
	}

	@Then("^Verifico diligenciamiento exitoso$")
	public void verifico_diligenciamiento_exitoso() {
	   
		DiligenciarCanonSteps.verificar_ingreso_datos_formulario_exitoso();
	}
	
	
	@Then("^Verifico que se presente alerta de validación$")
	public void verifico_que_se_presente_alerta_de_validación(){
		
		DiligenciarCanonSteps.verificar_ingreso_datos_formulario_con_errores();
	    
	}

}
