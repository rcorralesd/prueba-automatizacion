package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")

public class AbrirNavegadorPage extends PageObject{

	//Opción Productos y Servicios
	@FindBy (xpath="//*[@id=\'main-menu\']/div[2]/ul[1]/li[3]/a")
	public WebElementFacade opcProdServi;
	
	//Opción Leasing
	@FindBy (xpath="//*[@id=\'productosPersonas\']/div/div[1]/div/div/div[2]/div/a")
	public WebElementFacade opcLeasing;
	
	//Label de la opcion leasing a verificar
    @FindBy (xpath="//*[@id=\'main-content\']/div[1]/div/div[2]/div/div/h1")
    public WebElementFacade lblHomeLea;
      
    //Opción Leasing habitacional
  	@FindBy (xpath="//*[@id=\'category-detail\']/div/div/div[2]/div/div[2]/h2/a")
  	public WebElementFacade opcLeasingHabitacional;
  	
    //Label de la opcion leasing habitacional a verificar
    @FindBy (xpath="//*[@id=\'main-content\']/div[1]/div/div[2]/div/div/h1")
    public WebElementFacade lblHomeLeaHab;
    
   //Opción Canon Constante
  	@FindBy (xpath="//*[@id=\'main-content\']/div[4]/div/div/div/div/div[1]/div/div/div[1]/a")
  	public WebElementFacade opcCanonConstante;
  	
    //Label la opcion canon constante a verificar
    @FindBy (xpath="//*[@id=\'for-detail\']/div[1]/h1")
    public WebElementFacade lblCanonConstante;
	
	
	public void opcion_prod_y_ser() {
		opcProdServi.click();
	}
	
	
	public void opcion_leasing() {
		opcLeasing.click();
	}
	
	
	public void validar_leasing() {
		String labelv = "Leasing";
		String strMensaje = lblHomeLea.getText();
		assertThat(strMensaje, containsString(labelv));
	}
	
	public void opcion_leasing_habitacional() {
		opcLeasingHabitacional.click();
	}
	
	public void validar_leasing_habitacional() {
		String labelv = "Leasing Habitacional";
		String strMensaje = lblHomeLeaHab.getText();
		assertThat(strMensaje, containsString(labelv));
	}
	
	public void opcion_canon_constante() {
		opcCanonConstante.click();
	}
	
	public void validar_canon_constante() {
		String labelv = "Simulador de canon financiero";
		String strMensaje = lblCanonConstante.getText();
		assertThat(strMensaje, containsString(labelv));
	}
	
	
}
