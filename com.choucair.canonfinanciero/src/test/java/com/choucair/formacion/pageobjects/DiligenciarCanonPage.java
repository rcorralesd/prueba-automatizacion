package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DiligenciarCanonPage extends PageObject {
	
	//Campo Valor del activo
	@FindBy (xpath="//*[@id=\'sim-detail\']/form/div[1]/input")
	public WebElementFacade txtValorActivo;
	
	//Campo Plazo
	@FindBy (xpath="//*[@id=\'sim-detail\']/form/div[2]/input")
	public WebElementFacade txtPlazo;
	
	//Campo Porcentaje opción de Compra
	@FindBy (xpath="//*[@id=\'sim-detail\']/form/div[3]/input")
	public WebElementFacade txtPorcentaje;
	
	//Campo Selección Tipo de Tasa
	@FindBy (xpath="//*[@id=\'sim-detail\']/form/div[4]/select")
	public WebElementFacade cmbTipoTasa;
	
	//Campo Modalidad
	@FindBy (xpath="(//*[@id=\'sim-detail\']/form/div[5]/input")
	public WebElementFacade lblModalidad;
	
	//Botón Simular
	@FindBy (xpath="//*[@id=\'sim-detail\']/form/div[6]/button")
	public WebElementFacade btnSimular;
	
	//Label Resultado Simulación
    @FindBy (xpath="//*[@id=\'resultado\']/h2")
    public WebElementFacade lblResultado;
    
    //Mensaje de Error
  	@FindBy (xpath="(//DIV[@class='error text-danger bg-danger'])[1]")
  	public WebElementFacade lblMensajeError;
  	
  //Campo valor canon de la tabla
  	@FindBy (xpath="//*[@id=\'resultado\']/div/table/tbody/tr[1]/td[2]")
  	public WebElementFacade lblValorCanon;
  
  //Campo tasa efectiva anual
  	@FindBy (xpath="//*[@id=\'resultado\']/div/table/tbody/tr[2]/td[2]")
  	public WebElementFacade lblTasaEfectiva;
  	
  //Campo Tasa Nominal
  	@FindBy (xpath="//*[@id=\'resultado\']/div/table/tbody/tr[3]/td[2]")
  	public WebElementFacade lblTasaNominal;
  	
  //Campo valor opción de compra
  	@FindBy (xpath="//*[@id=\'resultado\']/div/table/tbody/tr[4]/td[2]")
  	public WebElementFacade lblValorOpCompra;
  	
  //Campo porcentaje de la opción de compra
  	@FindBy (xpath="//*[@id=\'resultado\']/div/table/tbody/tr[5]/td[2]")
  	public WebElementFacade lblPorcentajeOpcion;
  	
  	
	
	public void ValorActivo(String datoPrueba) {
		txtValorActivo.click();
		txtValorActivo.clear();
		txtValorActivo.sendKeys(datoPrueba);
	}

	
	public void Plazo(String datoPrueba) {
		txtPlazo.click();
		txtPlazo.clear();
		txtPlazo.sendKeys(datoPrueba);
	}
	
	
	public void Porcentaje(String datoPrueba) {
		txtPorcentaje.click();
		txtPorcentaje.clear();
		txtPorcentaje.sendKeys(datoPrueba);
	}
	
	public void Select_Tipo_Tasa(String datoPrueba) {
		cmbTipoTasa.click();
		cmbTipoTasa.selectByVisibleText(datoPrueba);
	}
	
	
	public void Simular(){
		//if (lblModalidad.getText() == "Vencida"){
	    btnSimular.click();	
		//}
	}
	
	
	public void diligenciamiento_sin_errores() {
		String labelv = "Resultado de la simulación";
		String strMensaje = lblResultado.getText();
		assertThat(strMensaje, containsString(labelv));
		
		String strValor = lblValorCanon.getText();
		String strTasaAnual = lblTasaEfectiva.getText();
		String strTasaNominal = lblTasaNominal.getText();
		String strValorOpcionCompra = lblValorOpCompra.getText();
		String strPorceOpCompra = lblPorcentajeOpcion.getText();
		
		System.out.println(strValor);
		System.out.println(strTasaAnual);
		System.out.println(strTasaNominal);
		System.out.println(strValorOpcionCompra);
		System.out.println(strPorceOpCompra);
	}
	
	
	
	public void diligenciamiento_con_errores() {
	assertThat(lblMensajeError.isCurrentlyVisible(), is(true));
   }
	
	
}
